FROM ubuntu:22.04
ARG ecbuild_version=3.8.0
ARG eccodes_version=2.31.0
ARG eckit_version=1.24.4
ARG metkit_version=1.10.15
ARG fdb_version=5.11.17
RUN apt update && apt install -y git cmake g++ gfortran python3 python3-pip
RUN mkdir -p /src
WORKDIR /src
RUN git clone https://github.com/ecmwf/ecbuild --depth 1 -b $ecbuild_version
ENV PATH="${PATH}:/src/ecbuild/bin"
RUN git clone https://github.com/ecmwf/eccodes --depth 1 -b $eccodes_version
RUN git clone https://github.com/ecmwf/eckit --depth 1 -b $eckit_version
RUN git clone https://github.com/ecmwf/metkit --depth 1 -b $metkit_version
RUN git clone https://github.com/ecmwf/fdb --depth 1 -b $fdb_version
RUN mkdir /src/eccodes/build && cd /src/eccodes/build && cmake .. -DENABLE_AEC=OFF && make && make install && pip3 install eccodes
RUN mkdir /src/eckit/build && cd /src/eckit/build && ecbuild -- .. && make && make install
RUN mkdir /src/metkit/build && cd /src/metkit/build && ecbuild -- .. && make && make install
RUN mkdir /src/fdb/build && cd /src/fdb/build && ecbuild -- .. && make && make install
RUN apt install -y nlohmann-json3-dev
#RUN python3 -m pip install ipython
#ENV LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:/usr/local/lib"
#COPY dev /src/zarrcodes
#RUN mkdir /src/zarrcodes/build && cd /src/zarrcodes/build && cmake .. && make && make install
RUN mkdir /data
WORKDIR /data
ENV FDB5_CONFIG_FILE=/data/config/config.yaml
ENV FDB_ROOT_DIRECTORY=/data/data
