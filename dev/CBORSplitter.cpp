#include "eckit/io/PeekHandle.h"
#include "eckit/message/Message.h"

#include "CBORSplitter.hpp"

#include <nlohmann/json.hpp>
using json = nlohmann::json;

#include <iostream>

namespace zarrcodes {
    bool CBORMessageDecoder::match(const eckit::message::Message& msg) const {
        size_t len = msg.length();
        const char* p = static_cast<const char*>(msg.data());
        return (len >= 16 and p[0] == 'C' and p[1] == 'B' and p[2] == 'O' and p[3] == 'R');
    }

    void CBORMessageDecoder::print(std::ostream& s) const {
        s << "CBORMessageDecoder[]";
    }

    void CBORMessageDecoder::getMetadata(const eckit::message::Message& ecmsg,
                                         eckit::message::MetadataGatherer& gatherer,
                                         const eckit::message::GetMetadataOptions&) const {
        size_t len = ecmsg.length();
        const char* p = static_cast<const char*>(ecmsg.data());

        auto msg = json::from_cbor(p + 16, p + len);
        std::cerr << "CBOR next: " << msg << std::endl;
        for (auto& [key, val] : msg.items()) {
            if (val.is_number_integer() ) {
                gatherer.setValue(key, val.get<long>());
            } else if (val.is_number_float() ) {
                gatherer.setValue(key, val.get<double>());
            } else if (val.is_string()) {
                gatherer.setValue(key, val.get<std::string>());
            }
        }
    }

    eckit::Buffer CBORMessageDecoder::decode(const eckit::message::Message& msg) const {
        return eckit::Buffer(); // TODO: move payload into buffer
    }


    CBORMessageContent::CBORMessageContent(std::vector<unsigned char> content) : content_(std::move(content)) {

    }

    CBORMessageContent::~CBORMessageContent() {

    }

    size_t CBORMessageContent::length() const {
        return content_.size();
    }

    void CBORMessageContent::print(std::ostream & s) const {
        s << "CBORMessageContent[]";
    }

    const void* CBORMessageContent::data() const {
        return static_cast<const void*>(content_.data());
    }


    CBORSplitter::CBORSplitter(eckit::PeekHandle& handle) : Splitter(handle) {}

    CBORSplitter::~CBORSplitter() {}

    eckit::message::Message CBORSplitter::next() {
        auto buffer = std::vector<unsigned char>(16);
        long readsize = handle_.read(buffer.data(), buffer.size());
        if (readsize < 16) {
            std::cerr << "CBOR next, couldn't read header" << std::endl;
            return {};
        }

        uint32_t flags = uint32_t(buffer[4]) << 0
                       | uint32_t(buffer[5]) << 8
                       | uint32_t(buffer[6]) << 16
                       | uint32_t(buffer[7]) << 24;
        uint64_t size = uint64_t(buffer[8]) << 0
                      | uint64_t(buffer[9]) << 8
                      | uint64_t(buffer[10]) << 16
                      | uint64_t(buffer[11]) << 24
                      | uint64_t(buffer[12]) << 32
                      | uint64_t(buffer[13]) << 40
                      | uint64_t(buffer[14]) << 48
                      | uint64_t(buffer[15]) << 56;
        std::cerr << "CBOR next " << flags << " " << size << std::endl;
        buffer.resize(16 + size);
        readsize = handle_.read(buffer.data() + 16, buffer.size() - 16);
        if (readsize < size) {
            std::cerr << "CBOR next, couldn't read payload" << std::endl;
            return {};
        }
        return eckit::message::Message(new CBORMessageContent(std::move(buffer)));
    }

    void CBORSplitter::print(std::ostream& s) const{
        s << "CBORSplitter[]";
    }
}

namespace eckit {
namespace message {

template <>
bool SplitterBuilder<zarrcodes::CBORSplitter>::match(eckit::PeekHandle& handle) const {

    unsigned char c0 = handle.peek(0);
    unsigned char c1 = handle.peek(1);
    unsigned char c2 = handle.peek(2);
    unsigned char c3 = handle.peek(3);

    if (c0 == 'C' and c1 == 'B' and c2 == 'O' and c3 == 'R') {
        return true;
    }

    return false;
}

}  // namespace message
}  // namespace eckit

static eckit::message::SplitterBuilder<zarrcodes::CBORSplitter> splitter;
static zarrcodes::CBORMessageDecoder decoder;
