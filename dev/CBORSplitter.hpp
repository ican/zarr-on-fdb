#pragma once

#include "eckit/message/MessageContent.h"
#include "eckit/message/Splitter.h"

namespace zarrcodes {
    class CBORMessageDecoder : public eckit::message::MessageDecoder {
        private:
            bool match(const eckit::message::Message&) const override;
            void print(std::ostream&) const override;

            void getMetadata(const eckit::message::Message& msg,
                             eckit::message::MetadataGatherer&,
                             const eckit::message::GetMetadataOptions&) const override;

            eckit::Buffer decode(const eckit::message::Message& msg) const override;
    };

    class CBORMessageContent : public eckit::message::MessageContent {
        public:
            CBORMessageContent(std::vector<unsigned char>);
            //explicit CBORMessageContent(const codes_handle* handle);

            ~CBORMessageContent();
        private:
            size_t length() const override;
            void print(std::ostream & s) const override;
            std::vector<unsigned char> content_;
            const void* data() const override;
    };

    class CBORSplitter : public eckit::message::Splitter {
        public:
            CBORSplitter(eckit::PeekHandle& handle);
            ~CBORSplitter();

        private:
            eckit::message::Message next() override;
            void print(std::ostream&) const override;
    };
}
