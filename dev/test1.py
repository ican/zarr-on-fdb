import pyfdb; fdb = pyfdb.FDB()
import cffi; ffi = cffi.FFI(); ffi.dlopen("zarrcodes")

import cbor2
import struct
def cborpack(o):
    pack = cbor2.dumps(o)
    return struct.pack("<4sIQ", b"CBOR", 0, len(pack)) + pack

def cborunpack(res):
    header = res.read(16)
    if len(header) < 16:
        raise ValueError("no header found")
    magic, flags, size = struct.unpack("<4sIQ", header)
    assert magic == b"CBOR"
    return cbor2.loads(res.read(size))

#import json
#fdb.archive(cborpack({"dataset": "test", "metakey": ".zgroup", "variable": "foo", "payload": json.dumps({}).encode("utf-8")}))


def zarr2fdb_key(k):
    parts = k.split("/")
    if parts[-1].startswith(".z"):
        if len(parts) == 2:
            return {"is_meta": "yes", "dataset": parts[0], "metakey": parts[1]}
        elif len(parts) == 3:
            return {"is_meta": "yes", "dataset": parts[0], "variable": parts[1], "metakey": parts[2]}
        else:
            raise ValueError()
    else:
        if "." in parts[-1]:
            parts = parts[:-1] + parts[-1].split(".")
        parts = parts[:2] + [int(p) for p in parts[2:]]
        return {
            "is_meta": "no", "dimcount": len(parts) - 2,
            **dict(zip(["dataset", "variable", "chunk1", "chunk2", "chunk3", "chunk4"], parts))
        }

def fdb2zarr_key(k):
    if not "dataset" in k:
        return ""
    key = k["dataset"]
    if "variable" in k and k["variable"]:  #non-existing variable key is returned as empty string
        key += "/" + k["variable"]
    if k.get("is_meta", "no") == "yes":
        key += "/" + k["metakey"]
    else:
        key += "/" + ".".join(k[f"chunk{i+1}"] for i in range(int(k["dimcount"])))
    return key

class FDBStore:
    def __init__(self, fdb, dataset):
        self.fdb = fdb
        self.dataset = dataset
        self.dirty_keys = set()
        self.max_dirty = 10000
        self.metacache = {}

    def __setitem__(self, key, value):
        print(f"store {key}", flush=True)
        self.mark_dirty(key)
        if self.is_metakey(key):
            self.metacache[key] = value
        fdbkey = zarr2fdb_key(self.dataset + "/" + key)
        fdb.archive(cborpack({**fdbkey, "payload": bytes(value)}))
    def __delitem__(self, key):
        print(f"delete {key}", flush=True)
        self.mark_dirty(key)
        if self.is_metakey(key):
            self.metacache[key] = None
        fdbkey = zarr2fdb_key(self.dataset + "/" + key)
        fdb.archive(cborpack({**fdbkey}))
    def __getitem__(self, key):
        if self.is_metakey(key):
            try:
                value = self.metacache[key]
            except KeyError:
                pass
            else:
                if value is None:
                    raise KeyError(key)
                else:
                    print(f"get {key} from cache", flush=True)
                    return value
        print(f"get {key} from FDB", flush=True)
        if key in self.dirty_keys:
            self.flush()
        if not isinstance(key, str):
            raise KeyError(key)
        fdbkey = zarr2fdb_key(self.dataset + "/" + key)
        res = fdb.retrieve(fdbkey)
        try:
            cbordata = cborunpack(res)
        except ValueError:
            raise KeyError(key)
        try:
            value = cbordata["payload"]  # TODO: verify inline key
        except KeyError:
            raise KeyError(key)
        self.metacache[key] = value
        return value
    def __contains__(self, key):
        print(f"contains {key}", flush=True)
        try:
            self[key]
        except KeyError:
            return False
        return True

    @staticmethod
    def is_metakey(key):
        return key.split("/")[-1].startswith(".z")

    # these implementations would require deletion support in fdb
    #def __iter__(self):
    #    for it in fdb.list({"dataset": self.dataset}, True, True):
    #        k = fdb2zarr_key(it["keys"])
    #        yield k.split("/", 1)[-1]
    #def __len__(self):
    #    return sum(1 for _ in fdb.list({"dataset": self.dataset}, True))

    def __iter__(self):
        if len(self.dirty_keys) > 0:
            self.flush()
        for it in fdb.list({"dataset": self.dataset}, True, True):
            k = fdb2zarr_key(it["keys"]).split("/", 1)[-1]
            try:
                self[k]
            except KeyError:
                continue
            yield k
    def __len__(self):
        return sum(1 for _ in self)


    def mark_dirty(self, key):
        self.dirty_keys.add(key)
        if len(self.dirty_keys) > self.max_dirty:
            self.flush()
    def flush(self):
        print("flushing...", flush=True)
        self.fdb.flush()
        self.dirty_keys = set()


import zarr
#store = zarr.storage.KVStore(FDBStore(fdb, "foo"))
#
#store["bar/0.1"] = b"test12345"
#store["bar/.zfoo"] = b"some_metadata"
#
#fdb.flush()
#
#print(store["bar/0.1"])
#print(list(store))


import xarray as xr

store = zarr.storage.KVStore(FDBStore(fdb, "testds"))

def main():
    ds = xr.Dataset({
        "x": [1, 2, 3, 4],
    })

    ds.to_zarr(store)


    print(xr.open_dataset(store, engine="zarr"))

if __name__ == "__main__":
    #main()
    pass
