#include <fdb5/api/FDB.h>
#include <eckit/runtime/Main.h>

int main(int argc, char** argv) {
    eckit::Main::initialise(argc, argv);
    auto fdb = fdb5::FDB{};
    fdb.archive(fdb5::Key({{"a", "1"}, {"b", "2"}, {"c", "4"}}), "foo", 4);  // works fine
    fdb.archive(fdb5::Key({{"a", "1"}, {"b", "2"}, {"d", "4"}}), "bar", 4);  // crashes
}
